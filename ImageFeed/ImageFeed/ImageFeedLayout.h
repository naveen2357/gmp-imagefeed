//
//  ImageFeedLayout.h
//  ImageFeed
//
//  Created by Naveen Chaudhary on 30/07/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageFeedLayoutProtocol <NSObject>
-(CGFloat)calculateHeightForItemAtIndexPath:(NSIndexPath *)indexPath withWidth:(CGFloat)width;
@end

@interface ImageFeedLayout : UICollectionViewLayout
@property (nonatomic, weak) id<ImageFeedLayoutProtocol> delegate;
@end
