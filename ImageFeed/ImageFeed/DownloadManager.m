//
//  DownloadManager.m
//  ImageFeed
//
//  Created by Naveen Chaudhary on 30/07/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "DownloadManager.h"
#import "CacheManager.h"
#import "CloudinaryManager.h"
#import "FeedDataManager.h"

static DownloadManager *sharedInstance = nil;

@implementation DownloadManager

+ (id)sharedManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DownloadManager alloc] init];
    });
    return sharedInstance;
}

#pragma mark - public methods
- (void)uploadImage:(UIImage *)image withName:(NSString *)name andCallback:(void (^)(BOOL))callBack {
    [[CloudinaryManager sharedManager] uploadImage:image withName:name andCallback:^(BOOL success) {
        [[CacheManager sharedManager] saveImage:image forKey:name];
        callBack(success);
    }];
}

- (void)imageFromUrl:(NSString *)url withCallback:(void (^)(UIImage *image))callBack {
    UIImage *image = [[CacheManager sharedManager] imageForKey:url];
    if (image) {
        callBack (image);
    } else {
        [[CloudinaryManager sharedManager] imageFromUrl:url withCallback:^(UIImage *image) {
            callBack(image);
            [[CacheManager sharedManager] saveImage:image forKey:url];
        }];
    }
}

- (void)downloadFeedWithExitingDataCount:(NSUInteger)dataCount withCallBack:(void (^)(NSArray *arrPhotos))callBack {
    [[CloudinaryManager sharedManager] downloadFeedWithExitingDataCount:dataCount withCallBack:^(NSArray *arrPhotos) {
        callBack(arrPhotos);
    }];
}

@end
