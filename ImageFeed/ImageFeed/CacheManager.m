//
//  CacheManager.m
//  ImageFeed
//
//  Created by Naveen Chaudhary on 30/07/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "CacheManager.h"

static CacheManager *sharedInstance = nil;

@implementation CacheManager

+ (id)sharedManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CacheManager alloc] init];
    });
    return sharedInstance;
}

#pragma mark - public methods
- (void)saveImage:(UIImage *)image forKey:(NSString *)key {
    if (image) {
        [self setObject:image forKey:key];
    }
}

- (UIImage *)imageForKey:(NSString *)key {
    return [self objectForKey:key];
}

@end
