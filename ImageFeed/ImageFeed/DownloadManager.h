//
//  DownloadManager.h
//  ImageFeed
//
//  Created by Naveen Chaudhary on 30/07/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DownloadManager : NSObject

+ (id)sharedManager;
- (void)uploadImage:(UIImage *)image withName:(NSString *)name andCallback:(void (^)(BOOL success))callBack;
- (void)imageFromUrl:(NSString *)url withCallback:(void (^)(UIImage *image))callBack;
- (void)downloadFeedWithExitingDataCount:(NSUInteger)dataCount withCallBack:(void (^)(NSArray *arrPhotos))callBack;
@end
