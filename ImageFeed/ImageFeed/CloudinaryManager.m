//
//  CloudinaryManager.m
//  ImageFeed
//
//  Created by Naveen Chaudhary on 30/07/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "CloudinaryManager.h"
#import "Cloudinary-Swift.h"

static CloudinaryManager *sharedInstance = nil;
static NSString *cloudName = @"dxwrdt9xk";
static NSString *apiKey = @"592278428648128";
static NSString *apiSecret = @"j1CdN1vt3XrbAa8Fer__5Qk7Ic8";
static NSString *uploadPreset = @"hzdwdchm";
static NSString *tags = @"imageFeedUpload";
static NSInteger maxImageWidth = 200;

@interface CloudinaryManager ()
@property (nonatomic) CLDCloudinary *cloudinary;
@end

@implementation CloudinaryManager

+ (id)sharedManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CloudinaryManager alloc] init];
        
        CLDConfiguration *config =  [[CLDConfiguration alloc] initWithCloudName:cloudName apiKey:apiKey apiSecret:apiSecret privateCdn:NO secure:YES cdnSubdomain:NO secureCdnSubdomain:NO secureDistribution:nil cname:nil uploadPrefix:nil];
        sharedInstance.cloudinary = [[CLDCloudinary alloc] initWithConfiguration:config networkAdapter:nil sessionConfiguration:nil];
        
        NSLog(@"cloudinary created");
    });
    return sharedInstance;
}

#pragma mark - public methods
- (void)downloadFeedWithExitingDataCount:(NSUInteger)dataCount withCallBack:(void (^)(NSArray *))callBack {
    
    NSString *urlString = [NSString stringWithFormat:@"http://res.cloudinary.com/%@/image/list/%@.json", cloudName, tags];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (data) {
                    NSDictionary *photosJSON = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                    NSArray *fetchedPhotos = photosJSON[@"resources"];

                    callBack(fetchedPhotos);
                } else {
                    callBack(@[]);
                }
            }] resume];
}

- (void)imageFromUrl:(NSString *)url withCallback:(void (^)(UIImage *))callBack {
    
    CLDTransformation *transformation = [[CLDTransformation alloc] init];
    [transformation setWidthWithInt:maxImageWidth];
    CLDUrl *cldUrl = [self.cloudinary createUrl];
    cldUrl = [cldUrl setTransformation:transformation];
    
    url = [cldUrl generate:[NSString stringWithFormat:@"%@.jpg", url] signUrl:NO];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:url]
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    callBack(image);
                } else {
                    callBack(NULL);
                }
            }] resume];
}

- (void)uploadImage:(UIImage *)image withName:(NSString *)name andCallback:(void (^)(BOOL))callBack {
    CLDUploadRequestParams *params = [[CLDUploadRequestParams alloc] init];
    [params setPublicId:name];
    [params setTags:tags];
    [[self.cloudinary createUploader] uploadWithData:UIImageJPEGRepresentation(image, 0.8) uploadPreset:uploadPreset params:params progress:^(NSProgress * _Nonnull progress) {
        NSLog(@"%@",progress.localizedDescription);
    } completionHandler:^(CLDUploadResult * _Nullable result, NSError * _Nullable error) {
        if (error) {
            callBack(NO);
        } else {
            callBack(YES);
        }
    }];
}

@end
