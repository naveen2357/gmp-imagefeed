//
//  ImageFeedCell.m
//  ImageFeed
//
//  Created by Naveen Chaudhary on 30/07/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "ImageFeedCell.h"

@interface ImageFeedCell ()
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@end

@implementation ImageFeedCell
- (void)setCellImage:(UIImage *)image {
    [self.imageView setImage:image];
}
@end
