//
//  FeedController.m
//  ImageFeed
//
//  Created by Naveen Chaudhary on 30/07/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "FeedController.h"
#import "DownloadManager.h"
#import <Photos/Photos.h>
#import "ImageFeedLayout.h"
#import "ImageFeedCell.h"

@interface FeedController ()<UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ImageFeedLayoutProtocol>
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIButton *addButton;
@property (nonatomic) NSArray *arrImages;
@end

@implementation FeedController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    ImageFeedLayout *feedLayout = (ImageFeedLayout *)self.collectionView.collectionViewLayout;
    feedLayout.delegate = self;
    
    [self updateAddNewImageButton];
    [self fetchImageFeed];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods
- (void)updateAddNewImageButton {
    [self.addButton.layer setCornerRadius:self.addButton.bounds.size.width/2];
}

- (IBAction)addNewImageButtonTapped:(id)sender {
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = NO;
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:pickerView animated:YES completion:^{
        
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.arrImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ImageFeedCellIdentifier";
    ImageFeedCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell setCellImage:nil];
    
    NSDictionary *photoDict = self.arrImages[indexPath.item];
    
    NSUInteger tag = indexPath.item;
    [cell setTag:tag];
    
    [[DownloadManager sharedManager] imageFromUrl:photoDict[@"public_id"] withCallback:^(UIImage *image) {
        if (tag == cell.tag) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [cell setCellImage:image];
            });
        }
    }];

    return cell;
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
    UIImage * img = [info valueForKey:UIImagePickerControllerOriginalImage];

    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[refURL] options:nil];
    NSString *imageName = [[result firstObject] filename];
 
    [self uploadImage:img withName:imageName];
}

#pragma mark - network operations
- (void)uploadImage:(UIImage *)image withName:(NSString *)imgName {
    typeof(self) __weak weakSelf = self;
    [[DownloadManager sharedManager] uploadImage:image withName:imgName andCallback:^(BOOL success) {
        if (success) {
            [weakSelf fetchImageFeed];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Upload Error!" message:@"Image Upload Request could not be completed. Please try again." preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [weakSelf presentViewController:alertController animated:YES completion:nil];
        }
    }];
}

- (void)fetchImageFeed {
    typeof(self) __weak weakSelf = self;
    [[DownloadManager sharedManager] downloadFeedWithExitingDataCount:0 withCallBack:^(NSArray *arrPhotos) {
        weakSelf.arrImages = arrPhotos;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.collectionView reloadData];
        });
    }];
}

#pragma mark - ImageFeedLayoutProtocol
- (CGFloat)calculateHeightForItemAtIndexPath:(NSIndexPath *)indexPath withWidth:(CGFloat)width {
    NSDictionary *photo = [self.arrImages objectAtIndex:indexPath.item];
    return [photo[@"height"] floatValue]*width/[photo[@"width"] floatValue];
}

@end
