//
//  ImageFeedCell.h
//  ImageFeed
//
//  Created by Naveen Chaudhary on 30/07/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageFeedCell : UICollectionViewCell

- (void)setCellImage:(UIImage *)image;
@end
